import { ContainerNavigator } from "@navigation";
import React from "react";
import "react-native-gesture-handler";
import { SafeAreaProvider } from "react-native-safe-area-context";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";

import Toast from "react-native-toast-message";

import { persistor, store } from "@store";

const App = () => (
  <>
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <SafeAreaProvider>
          <ContainerNavigator />
        </SafeAreaProvider>
      </PersistGate>
    </Provider>
    <Toast position="bottom" />
  </>
);
export default App;
