export default {
  orange: "#FF9800",
  white: "#FFFFFF",
  light: "#F8F9FB",
  border: "rgba(0, 0, 0, 0.23)",
  grey: "#757575",
  green: "#4CAF50",
  darkBlue: "#3949AB",
  red: "#F44336",
};
