import { auth, app as firebaseApp } from "./firebase";

export { auth, firebaseApp };
