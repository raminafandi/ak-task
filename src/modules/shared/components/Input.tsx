import { Pressable, StyleSheet, Text, TextInput, View } from "react-native";
import React, { FunctionComponent } from "react";
import { Colors } from "@shared/constants";
import { Entypo } from "@expo/vector-icons";

type Props = React.ComponentProps<typeof TextInput> & {
  title: string;
  icon: keyof typeof Entypo.glyphMap;
  placeholder: string;
};

const Input: FunctionComponent<Props> = ({
  title,
  placeholder,
  icon,
  ...props
}: Props) => {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>{title}</Text>
      <View style={styles.inputContainer}>
        <Entypo name={icon} size={18} color="black" style={styles.icon} />
        <TextInput style={styles.input} placeholder={placeholder} {...props} />
      </View>
    </View>
  );
};

export default Input;

const styles = StyleSheet.create({
  container: {
    margin: 10,
    padding: 15,
    borderWidth: 1,
    borderColor: Colors.border,
    borderRadius: 4,
    backgroundColor: Colors.light,
  },
  text: {
    position: "absolute",
    top: -10,
    left: 10,
    backgroundColor: Colors.light,
    paddingHorizontal: 5,
    fontSize: 12,
  },
  inputContainer: {
    flexDirection: "row",
    alignItems: "center",
  },
  icon: {
    marginRight: 10,
  },
  input: {
    flex: 1,
    fontSize: 16,
  },
});
