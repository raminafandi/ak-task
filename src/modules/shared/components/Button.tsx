import {
  ActivityIndicator,
  Pressable,
  StyleSheet,
  Text,
  ViewStyle,
} from "react-native";
import React from "react";
import { Colors } from "@shared/constants";

type Props = {
  title: string;
  style?: ViewStyle;
  loading?: boolean;
  onPress?: () => void;
};

const Button = ({
  title,
  style,
  loading = false,
  onPress,
  ...props
}: Props) => {
  return (
    <Pressable onPress={onPress} style={[styles.container, style]} {...props}>
      {loading ? (
        <ActivityIndicator color={Colors.white} size={"small"} />
      ) : (
        <Text style={styles.title}>{title.toLocaleUpperCase()}</Text>
      )}
    </Pressable>
  );
};

export default Button;

const styles = StyleSheet.create({
  container: {
    padding: 8,
    backgroundColor: Colors.orange,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 5,
    shadowColor: "rgba(0,0,0)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  title: {
    color: Colors.white,
    fontSize: 15,
    fontWeight: "500",
  },
});
