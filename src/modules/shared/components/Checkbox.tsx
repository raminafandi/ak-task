import { Pressable, StyleSheet, Text, TextInput, View } from "react-native";
import React, { FunctionComponent } from "react";
import { Colors } from "@shared/constants";
import { Entypo } from "@expo/vector-icons";

type Props = React.ComponentProps<typeof Pressable> & {
  checked: boolean;
  setChecked: (checked: boolean) => void;
};

const Checkbox: FunctionComponent<Props> = ({
  checked,
  setChecked,
  ...props
}: Props) => {
  return (
    <Pressable style={styles.container} onPress={() => setChecked(!checked)}>
      {checked ? <Entypo name="check" size={13} color={Colors.grey} /> : null}
    </Pressable>
  );
};

export default Checkbox;

const styles = StyleSheet.create({
  container: {
    borderColor: Colors.grey,
    borderWidth: 1,
    borderRadius: 4,
    backgroundColor: Colors.light,
    width: 15,
    height: 15,
  },
});
