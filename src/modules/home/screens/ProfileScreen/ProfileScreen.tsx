import { Pressable, StyleSheet, Text, View, Image } from "react-native";
import React, { FunctionComponent, useState } from "react";
import { NativeStackScreenProps } from "@react-navigation/native-stack";
import { SafeAreaView } from "react-native-safe-area-context";
import { Colors, Strings } from "@shared/constants";
import { RootBottomTabsParamList } from "@home/navigation/RootBottomTabsNavigator";
import { Button } from "@shared/components";
import useAuthActions from "@auth/hooks/useAuthActions";
import { useTypedSelector } from "@shared/hooks";
import { Limit, Profile, QRCode } from "@home/components";

type Props = NativeStackScreenProps<RootBottomTabsParamList, "Profile">;

const ProfileScreen: FunctionComponent<Props> = ({ navigation }) => {
  const { user } = useTypedSelector((state) => state.auth);
  //random num between 0 and 5000
  const [randomNum, setRandomNum] = useState(Math.floor(Math.random() * 5000));
  return (
    <SafeAreaView style={styles.safe}>
      <Profile name={user.name} />
      <Limit num={randomNum} />
      <QRCode />
      <Button
        title={Strings.check}
        onPress={() => {
          setRandomNum(Math.floor(Math.random() * 5000));
        }}
      />
    </SafeAreaView>
  );
};

export default ProfileScreen;

const styles = StyleSheet.create({
  safe: {
    flex: 1,
    padding: 10,
    backgroundColor: Colors.light,
  },
});
