import ProfileScreen from "./ProfileScreen";
import ContactScreen from "./ContactScreen";
import CreditsScreen from "./CreditsScreen";
import PartnersScreen from "./PartnersScreen/PartnersScreen";

export { ProfileScreen, ContactScreen, CreditsScreen, PartnersScreen };
