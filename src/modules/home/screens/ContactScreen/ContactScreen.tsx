import { Pressable, StyleSheet, Text, View, Image } from "react-native";
import React, { FunctionComponent, useState } from "react";
import { NativeStackScreenProps } from "@react-navigation/native-stack";
import { SafeAreaView } from "react-native-safe-area-context";
import { Colors, Strings } from "@shared/constants";
import { RootBottomTabsParamList } from "@home/navigation/RootBottomTabsNavigator";

type Props = NativeStackScreenProps<RootBottomTabsParamList, "Contact">;

const ContactScreen: FunctionComponent<Props> = ({ navigation }) => {
  return (
    <SafeAreaView style={styles.safe}>
      <Text>Contact</Text>
    </SafeAreaView>
  );
};

export default ContactScreen;

const styles = StyleSheet.create({
  safe: {
    flex: 1,
    padding: 10,
    backgroundColor: Colors.light,
  },
});
