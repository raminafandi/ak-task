import { Image, Pressable, StyleSheet, Text, View } from "react-native";
import React, { FunctionComponent } from "react";
import { Colors, Strings } from "@shared/constants";

type Props = React.ComponentProps<typeof Pressable> & {
  name: string;
};

const Profile: FunctionComponent<Props> = ({ name }: Props) => {
  return (
    <Pressable style={styles.profile}>
      <Image
        style={styles.profileImage}
        source={require("../../../../../assets/man.png")}
      />
      <View style={styles.left}>
        <Text style={styles.hello}>{Strings.hello}</Text>
        <Text style={styles.profileName}>{name}</Text>
      </View>
    </Pressable>
  );
};

export default Profile;

const styles = StyleSheet.create({
  profile: {
    alignItems: "center",
    flexDirection: "row",
    paddingBottom: 10,
    borderBottomColor: Colors.grey,
    borderBottomWidth: 1,
  },
  profileImage: {
    width: 90,
    height: 90,
    borderRadius: 45,
  },
  profileName: {
    fontSize: 20,
    fontWeight: "bold",
  },
  hello: {
    fontSize: 12,
    color: Colors.grey,
  },
  left: {
    padding: 12,
  },
});
