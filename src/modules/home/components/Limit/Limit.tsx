import { Pressable, StyleSheet, Text, View } from "react-native";
import React, { FunctionComponent } from "react";
import { Colors, Strings } from "@shared/constants";

type Props = React.ComponentProps<typeof Pressable> & {
  num: number;
};

const Limit: FunctionComponent<Props> = ({ num }: Props) => {
  return (
    <View style={styles.container}>
      <Text style={styles.text1}>{Strings.limit}</Text>
      <Text style={styles.text2}>₼{num}</Text>
    </View>
  );
};

export default Limit;

const styles = StyleSheet.create({
  container: {
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    padding: 30,
  },
  text1: {
    fontSize: 24,
    fontWeight: "bold",
    color: Colors.grey,
  },
  text2: {
    fontSize: 24,
    fontWeight: "bold",
    color: Colors.green,
  },
});
