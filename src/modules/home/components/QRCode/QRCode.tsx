import { Image, Pressable, StyleSheet, Text, View } from "react-native";
import React, { FunctionComponent } from "react";
import { Colors, Strings } from "@shared/constants";

type Props = React.ComponentProps<typeof Pressable> & {};

const QRCode: FunctionComponent<Props> = ({ ...props }: Props) => {
  return (
    <Pressable style={styles.container} {...props}>
      <Image
        style={styles.image}
        source={require("../../../../../assets/qe.png")}
      />
    </Pressable>
  );
};

export default QRCode;

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    justifyContent: "center",
    padding: 30,
  },
  image: {
    width: 300,
    height: 300,
  },
});
