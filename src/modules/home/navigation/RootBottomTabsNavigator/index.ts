import RootBottomTabsNavigator from "./RootBottomTabsNavigator";
import { RootBottomTabsParamList } from "./types";

export { RootBottomTabsNavigator, RootBottomTabsParamList };
