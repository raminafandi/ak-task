import React, { FunctionComponent } from "react";

import { RootBottomTabsParamList } from "./types";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import {
  ContactScreen,
  CreditsScreen,
  PartnersScreen,
  ProfileScreen,
} from "@home/screens";
import { FontAwesome5 } from "@expo/vector-icons";

const Tab = createBottomTabNavigator<RootBottomTabsParamList>();

const RootBottomTabsNavigator: FunctionComponent = () => {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Tab.Screen
        name="Profile"
        component={ProfileScreen}
        options={{
          tabBarIcon: ({ focused, color, size }) => (
            <FontAwesome5 name="jedi-order" size={size} color={color} />
          ),
        }}
      />
      <Tab.Screen
        name="Partners"
        component={PartnersScreen}
        options={{
          tabBarIcon: ({ focused, color, size }) => (
            <FontAwesome5 name="mandalorian" size={size} color={color} />
          ),
        }}
      />
      <Tab.Screen
        name="Credits"
        component={CreditsScreen}
        options={{
          tabBarIcon: ({ focused, color, size }) => (
            <FontAwesome5 name="galactic-republic" size={size} color={color} />
          ),
        }}
      />
      <Tab.Screen
        name="Contact"
        component={ContactScreen}
        options={{
          tabBarIcon: ({ focused, color, size }) => (
            <FontAwesome5 name="headphones" size={size} color={color} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default RootBottomTabsNavigator;
