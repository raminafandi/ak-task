type RootBottomTabsParamList = {
  Profile: undefined;
  Partners: undefined;
  Credits: undefined;
  Contact: undefined;
};

export { RootBottomTabsParamList };
