import React, { FunctionComponent } from "react";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { RootBottomTabsNavigator } from "../RootBottomTabsNavigator";
import { RootDrawerParamList } from "./types";
import CustomDrawerContent from "./CustomDrawer";
import { Image } from "react-native";

const Drawer = createDrawerNavigator<RootDrawerParamList>();

const RootNavigator: FunctionComponent = () => {
  return (
    <Drawer.Navigator
      drawerContent={(props) => <CustomDrawerContent {...props} />}
      screenOptions={{
        headerRight: () => (
          <Image
            source={require("../../../../../assets/logo.png")}
            style={{ height: 15, marginRight: 10 }}
          />
        ),
        headerTitle: "",
      }}
    >
      <Drawer.Screen name="Root" component={RootBottomTabsNavigator} />
    </Drawer.Navigator>
  );
};

export default RootNavigator;
