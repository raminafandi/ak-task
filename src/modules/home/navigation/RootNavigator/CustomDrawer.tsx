import React from "react";
import { View, StyleSheet, Image, Text, Pressable } from "react-native";
import { DrawerContentScrollView } from "@react-navigation/drawer";
import { FontAwesome5 } from "@expo/vector-icons";
import { Colors, Strings } from "@shared/constants";
import { useTypedSelector } from "@shared/hooks";
import useAuthActions from "@auth/hooks/useAuthActions";

type Props = {
  navigation: any;
};

function CustomDrawerContent({ navigation, ...props }: Props) {
  const { user } = useTypedSelector((state) => state.auth);
  const { logout } = useAuthActions();
  return (
    <View style={styles.container}>
      <DrawerContentScrollView {...props}>
        <View style={styles.profileContainer}>
          <Image
            style={styles.profileImage}
            source={require("../../../../../assets/man.png")}
          />
          <View style={styles.left}>
            <Text style={styles.hello}>{Strings.hello}</Text>
            <Text style={styles.profileName}>{user.name}</Text>
          </View>
        </View>

        <Pressable style={styles.drawerItem}>
          <FontAwesome5 name="grin-alt" size={19} color="black" />
          <Text style={styles.drawerItemText}>{Strings.language}</Text>
        </Pressable>
        <Pressable style={styles.drawerItem}>
          <FontAwesome5 name="grin-alt" size={19} color="black" />
          <Text style={styles.drawerItemText}>{Strings.language}</Text>
        </Pressable>
        <Pressable style={styles.drawerItem}>
          <FontAwesome5 name="grin-alt" size={19} color="black" />
          <Text style={styles.drawerItemText}>{Strings.notifications}</Text>
        </Pressable>
        <Pressable style={styles.drawerItem} onPress={() => logout()}>
          <FontAwesome5 name="grin-alt" size={19} color="black" />
          <Text style={styles.drawerItemText}>{Strings.logout}</Text>
        </Pressable>
      </DrawerContentScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  profileContainer: {
    alignItems: "center",
    flexDirection: "row",
    paddingHorizontal: 30,
    paddingVertical: 20,
  },
  profileImage: {
    width: 58,
    height: 58,
    borderRadius: 29,
  },
  profileName: {
    fontSize: 20,
    fontWeight: "bold",
  },
  drawerItem: {
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 30,
    paddingVertical: 15,
  },
  drawerItemText: {
    marginLeft: 20,
    fontSize: 16,
    fontWeight: "500",
  },
  hello: {
    fontSize: 12,
    color: Colors.grey,
  },
  left: {
    padding: 12,
  },
});

export default CustomDrawerContent;
