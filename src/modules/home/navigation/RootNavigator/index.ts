import RootNavigator from "./RootNavigator";
import { RootDrawerParamList } from "./types";

export { RootNavigator, RootDrawerParamList };
