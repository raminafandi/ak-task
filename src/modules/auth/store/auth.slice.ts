import { PayloadAction, createSlice } from "@reduxjs/toolkit";

import { AuthState } from "../types";

const initialState: AuthState = {
  isLoggedIn: false,
  user: {
    uuid: "",
    name: "",
    email: "",
  },
  token: "",
};

const authSlice = createSlice({
  name: "auth",
  initialState: initialState,
  reducers: {
    login: (state) => {
      state.isLoggedIn = true;
    },
    logout: (state) => {
      state.isLoggedIn = false;
    },
    setUser: (state, action: PayloadAction<AuthState["user"]>) => {
      state.user = action.payload;
    },
    setToken: (state, action: PayloadAction<AuthState["token"]>) => {
      state.token = action.payload;
    },
  },
});

export default authSlice.reducer;
export const authActions = authSlice.actions;
