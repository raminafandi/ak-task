import { createNativeStackNavigator } from "@react-navigation/native-stack";
import React, { FunctionComponent } from "react";

import { AuthStackParamList } from "./types";
import { LoginScreen, OTPScreen, RegisterScreen } from "@auth/screens";

const AuthStack = createNativeStackNavigator<AuthStackParamList>();

const AuthNavigator: FunctionComponent = () => {
  return (
    <AuthStack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <AuthStack.Screen name={"Login"} component={LoginScreen} />
      <AuthStack.Screen name={"Register"} component={RegisterScreen} />
      <AuthStack.Screen name={"OTP"} component={OTPScreen} />
    </AuthStack.Navigator>
  );
};

export default AuthNavigator;
