import { AuthState } from "@auth/types";
import { IdTokenResult, User } from "firebase/auth";

class FirebaseUserMapper {
  static mapFirebaseUserToUser(firebaseUser: User): AuthState["user"] {
    return {
      email: firebaseUser.email,
      uuid: firebaseUser.uid,
      name: firebaseUser.displayName,
    };
  }
  static mapFirebaseTokenToToken(
    firebaseToken: IdTokenResult
  ): AuthState["token"] {
    return firebaseToken.token;
  }
}

export default FirebaseUserMapper;
