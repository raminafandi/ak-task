import LoginScreen from "./LoginScreen";
import RegisterScreen from "./RegisterScreen";
import OTPScreen from "./OTPScreen";

export { LoginScreen, RegisterScreen, OTPScreen };
