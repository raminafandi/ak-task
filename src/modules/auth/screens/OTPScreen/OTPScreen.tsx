import { StyleSheet, Text, View } from "react-native";
import React, { FunctionComponent, useState } from "react";
import { NativeStackScreenProps } from "@react-navigation/native-stack";
import { AuthStackParamList } from "@auth/navigation/AuthNavigator/types";
import { SafeAreaView } from "react-native-safe-area-context";
import { Colors, Strings } from "@shared/constants";
import { Button } from "@shared/components";
import { BottomText, ErrorText, RememberMe } from "@auth/components";
import { OTPInitialState } from "@auth/types";
import { Formik } from "formik";
import OTPTextInput from "react-native-otp-textinput";
import useAuthActions from "@auth/hooks/useAuthActions";
import { OTPSchema } from "@auth/config/schemas";

type Props = NativeStackScreenProps<AuthStackParamList, "OTP">;

const OTPScreen: FunctionComponent<Props> = ({ navigation }) => {
  const initialValues: OTPInitialState = {
    code: "",
  };
  const { login } = useAuthActions();

  return (
    <SafeAreaView style={styles.safe}>
      <Formik
        initialValues={initialValues}
        validationSchema={OTPSchema}
        onSubmit={(values) => {
          login();
        }}
        validateOnChange={false}
      >
        {({ handleChange, handleBlur, handleSubmit, values, errors }) => (
          <>
            <View style={styles.center}>
              <Text style={styles.title}>{Strings.sentOTP}</Text>

              <OTPTextInput
                inputCount={4}
                handleTextChange={handleChange("code")}
                containerStyle={styles.otpContainer}
                textInputStyle={styles.otpTInput}
                tintColor={Colors.orange}
              />
              <ErrorText text={errors.code} />

              <BottomText
                firstText={Strings.notSent}
                secondText={Strings.resend}
                onPress={() => navigation.navigate("Register")}
              />
            </View>
            <Button
              title={Strings.login}
              style={styles.button}
              onPress={handleSubmit}
            />
          </>
        )}
      </Formik>
    </SafeAreaView>
  );
};

export default OTPScreen;

const styles = StyleSheet.create({
  safe: {
    flex: 1,
    padding: 60,
    backgroundColor: Colors.light,
  },
  center: {
    flex: 1,
    justifyContent: "center",
  },
  title: {
    fontSize: 20,
    fontWeight: "400",
    margin: 10,
    textAlign: "center",
  },
  button: {
    justifyContent: "flex-end",
  },
  otpContainer: { marginVertical: 30 },
  otpTInput: {
    borderWidth: 1,
    borderColor: Colors.light,
    borderRadius: 4,
    padding: 10,
    fontSize: 20,
    color: Colors.grey,
  },
});
