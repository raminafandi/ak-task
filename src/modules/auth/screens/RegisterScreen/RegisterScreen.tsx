import { StyleSheet, Text, View } from "react-native";
import React, { FunctionComponent, useState } from "react";
import { NativeStackScreenProps } from "@react-navigation/native-stack";
import { AuthStackParamList } from "@auth/navigation/AuthNavigator/types";
import { SafeAreaView } from "react-native-safe-area-context";
import { Colors, Strings } from "@shared/constants";
import { Button, Input } from "@shared/components";
import { BottomText, ErrorText, RememberMe } from "@auth/components";
import { RegisterInitialState } from "@auth/types";
import { Formik } from "formik";
import { RegisterSchema } from "@auth/config/schemas";
import useAuthentication from "@auth/hooks/useAuthentication";

type Props = NativeStackScreenProps<AuthStackParamList, "Register">;

const RegisterScreen: FunctionComponent<Props> = ({ navigation }) => {
  const { authLoading, onRegister } = useAuthentication();
  const initialValues: RegisterInitialState = {
    name: "",
    email: "",
    password: "",
    passwordConfirmation: "",
  };

  return (
    <SafeAreaView style={styles.safe}>
      <Formik
        initialValues={initialValues}
        validationSchema={RegisterSchema}
        validateOnChange={true}
        onSubmit={(values) => {
          onRegister(values.name, values.email, values.password);
          navigation.navigate("OTP");
        }}
      >
        {({ handleChange, handleBlur, handleSubmit, values, errors }) => (
          <>
            <View style={styles.center}>
              <Text style={styles.title}>{Strings.signup}</Text>
              <Input
                title={Strings.personalInfo}
                placeholder={Strings.name}
                value={values.name}
                onChangeText={handleChange("name")}
                onBlur={handleBlur("name")}
                icon="mail"
                keyboardType="email-address"
                autoCapitalize="none"
                textContentType="emailAddress"
              />
              <ErrorText text={errors.name} />
              <Input
                title={Strings.email}
                placeholder={Strings.emailPlaceholder}
                value={values.email}
                onChangeText={handleChange("email")}
                onBlur={handleBlur("email")}
                icon="mail"
                keyboardType="email-address"
                autoCapitalize="none"
                textContentType="emailAddress"
              />
              <ErrorText text={errors.email} />
              <Input
                title={Strings.password}
                placeholder={Strings.passwordPlaceholder}
                value={values.password}
                onChangeText={handleChange("password")}
                onBlur={handleBlur("password")}
                icon="key"
                secureTextEntry
                keyboardType="default"
                autoCapitalize="none"
                textContentType="password"
              />
              <ErrorText text={errors.password} />
              <Input
                title={Strings.repeatPassword}
                placeholder={Strings.passwordPlaceholder}
                value={values.passwordConfirmation}
                onChangeText={handleChange("passwordConfirmation")}
                icon="key"
                secureTextEntry
                keyboardType="default"
                autoCapitalize="none"
                textContentType="password"
              />
              <ErrorText text={errors.passwordConfirmation} />
              <BottomText
                firstText={Strings.haveAccount}
                secondText={Strings.goAccount}
                onPress={() => navigation.navigate("Login")}
              />
            </View>

            <Button
              title={Strings.register}
              style={styles.button}
              onPress={handleSubmit}
              loading={authLoading}
            />
          </>
        )}
      </Formik>
    </SafeAreaView>
  );
};

export default RegisterScreen;

const styles = StyleSheet.create({
  safe: {
    flex: 1,
    padding: 60,
    backgroundColor: Colors.light,
  },
  center: {
    flex: 1,
    justifyContent: "center",
  },
  title: {
    fontSize: 20,
    fontWeight: "400",
    margin: 10,
  },
  button: {
    justifyContent: "flex-end",
  },
});
