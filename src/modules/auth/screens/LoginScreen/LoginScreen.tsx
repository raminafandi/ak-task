import { StyleSheet, Text, View } from "react-native";
import React, { FunctionComponent, useState } from "react";
import { NativeStackScreenProps } from "@react-navigation/native-stack";
import { AuthStackParamList } from "@auth/navigation/AuthNavigator/types";
import { SafeAreaView } from "react-native-safe-area-context";
import { Colors, Strings } from "@shared/constants";
import { Button, Input } from "@shared/components";
import { BottomText, ErrorText, RememberMe } from "@auth/components";
import { LoginInitialState } from "@auth/types";
import { Formik } from "formik";
import { LoginSchema } from "@auth/config/schemas";
import useAuthentication from "@auth/hooks/useAuthentication";

type Props = NativeStackScreenProps<AuthStackParamList, "Login">;

const LoginScreen: FunctionComponent<Props> = ({ navigation }) => {
  const { authLoading, onLogin } = useAuthentication();

  const initialValues: LoginInitialState = {
    email: "",
    password: "",
  };

  const [checked, setChecked] = useState<boolean>(false);
  return (
    <SafeAreaView style={styles.safe}>
      <Formik
        initialValues={initialValues}
        validationSchema={LoginSchema}
        validateOnChange={true}
        onSubmit={(values) => onLogin(values.email, values.password)}
      >
        {({ handleChange, handleBlur, handleSubmit, values, errors }) => (
          <>
            <View style={styles.center}>
              <Text style={styles.title}>{Strings.welcome}</Text>
              <Input
                title={Strings.email}
                placeholder={Strings.emailPlaceholder}
                value={values.email}
                onChangeText={handleChange("email")}
                onBlur={handleBlur("email")}
                icon="mail"
                keyboardType="email-address"
                autoCapitalize="none"
                textContentType="emailAddress"
              />
              <ErrorText text={errors.email} />
              <Input
                title={Strings.password}
                placeholder={Strings.passwordPlaceholder}
                value={values.password}
                onChangeText={handleChange("password")}
                onBlur={handleBlur("password")}
                icon="key"
                secureTextEntry
                keyboardType="default"
                autoCapitalize="none"
                textContentType="password"
              />
              <ErrorText text={errors.password} />
              <RememberMe checked={checked} setChecked={setChecked} />
              <BottomText
                firstText={Strings.noAccount}
                secondText={Strings.newAccount}
                onPress={() => navigation.navigate("Register")}
              />
            </View>
            <Button
              title={Strings.login}
              style={styles.button}
              onPress={handleSubmit}
              loading={authLoading}
            />
          </>
        )}
      </Formik>
    </SafeAreaView>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  safe: {
    flex: 1,
    padding: 60,
    backgroundColor: Colors.light,
  },
  center: {
    flex: 1,
    justifyContent: "center",
  },
  title: {
    fontSize: 20,
    fontWeight: "400",
    margin: 10,
  },
  button: {
    justifyContent: "flex-end",
  },
  error: {
    color: Colors.red,
    marginHorizontal: 10,
  },
});
