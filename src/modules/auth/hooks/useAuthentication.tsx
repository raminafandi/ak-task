import { useState } from "react";
import Toast from "react-native-toast-message";

import useAuthActions from "./useAuthActions";

import { signIn, signOut, signUp } from "@auth/api";
import { AuthError } from "firebase/auth";
import { FirebaseUserMapper } from "@auth/mappers";

const useAuthentication = () => {
  const [authLoading, setAuthLoading] = useState<boolean>(false);
  const [error, setError] = useState<AuthError | null>(null);
  const { login, setToken, setUser, logout } = useAuthActions();

  const onRegister = async (name: string, email: string, password: string) => {
    setAuthLoading(true);
    setError(null);
    try {
      const user = await signUp(name, email, password);
      const token = await user.getIdTokenResult();
      setUser(FirebaseUserMapper.mapFirebaseUserToUser(user));
      setToken(token.token);
      Toast.show({
        type: "success",
        text1: "Register Successful",
      });
    } catch (e: unknown) {
      console.error(e);
      setError(e as AuthError);
      Toast.show({
        type: "error",
        text1: "Register Failed",
        text2: "Please check your email and password",
      });
    }
    setAuthLoading(false);
  };

  const onLogin = async (email: string, password: string) => {
    setAuthLoading(true);
    setError(null);
    try {
      const user = await signIn(email, password);
      const token = await user.getIdTokenResult();
      setUser(FirebaseUserMapper.mapFirebaseUserToUser(user));
      setToken(token.token);
      Toast.show({
        type: "success",
        text1: "Login Successful",
      });
      login();
    } catch (e: unknown) {
      console.error(e);
      setError(e as AuthError);
      Toast.show({
        type: "error",
        text1: "Login Failed",
        text2: "Please check your email and password",
      });
    }
    setAuthLoading(false);
  };

  return {
    onRegister,
    onLogin,
    authLoading,
  };
};

export default useAuthentication;
