import { bindActionCreators } from "@reduxjs/toolkit";
import { useDispatch } from "react-redux";

import { authActions } from "../store";

import { AppDispatch } from "@store";

const useAuthActions = () => {
  const dispatch = useDispatch<AppDispatch>();

  return bindActionCreators(authActions, dispatch);
};

export default useAuthActions;
