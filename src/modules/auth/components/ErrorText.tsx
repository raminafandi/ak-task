import { StyleSheet, Text } from "react-native";
import React, { FunctionComponent } from "react";
import { Colors } from "@shared/constants";

type Props = React.ComponentProps<typeof Text> & {
  text: string;
};

const ErrorText: FunctionComponent<Props> = ({ text }: Props) =>
  text ? <Text style={styles.error}>{text}</Text> : null;
export default ErrorText;

const styles = StyleSheet.create({
  error: {
    color: Colors.red,
    marginHorizontal: 10,
    marginBottom: 10,
    fontSize: 12,
    fontStyle: "italic",
  },
});
