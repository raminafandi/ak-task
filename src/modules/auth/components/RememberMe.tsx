import { Pressable, StyleSheet, Text, TextInput, View } from "react-native";
import React, { FunctionComponent } from "react";
import { Colors, Strings } from "@shared/constants";
import { Checkbox } from "@shared/components";

type Props = React.ComponentProps<typeof Pressable> & {
  checked: boolean;
  setChecked: (checked: boolean) => void;
};

const RememberMe: FunctionComponent<Props> = ({
  checked,
  setChecked,
  ...props
}: Props) => {
  return (
    <View style={styles.container}>
      <Pressable style={styles.mContainer} onPress={() => setChecked(!checked)}>
        <Checkbox checked={checked} setChecked={setChecked} />
        <Text style={styles.text}>{Strings.rememberMe}</Text>
      </Pressable>
      <Pressable style={styles.mContainer}>
        <Text style={styles.text}>{Strings.forgetPass}</Text>
      </Pressable>
    </View>
  );
};

export default RememberMe;

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
    margin: 10,
  },
  mContainer: {
    flexDirection: "row",
    alignItems: "center",
  },
  text: {
    fontSize: 14,
    color: Colors.grey,
    marginLeft: 5,
  },
});
