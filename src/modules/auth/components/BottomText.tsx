import { Pressable, StyleSheet, Text, TextInput, View } from "react-native";
import React, { FunctionComponent } from "react";
import { Colors, Strings } from "@shared/constants";

type Props = React.ComponentProps<typeof Pressable> & {
  firstText: string;
  secondText: string;
  onPress: () => void;
};

const BottomText: FunctionComponent<Props> = ({
  firstText,
  secondText,
  onPress,
  ...props
}: Props) => {
  return (
    <Pressable style={styles.container} onPress={onPress}>
      <Text style={styles.text1}>{firstText}</Text>
      <Text style={styles.text2}>{secondText}</Text>
    </Pressable>
  );
};

export default BottomText;

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    margin: 10,
  },
  text1: {
    fontSize: 12,
    color: Colors.darkBlue,
    marginRight: 5,
  },
  text2: {
    fontSize: 12,
    color: Colors.orange,
    textDecorationLine: "underline",
    marginLeft: 5,
  },
});
