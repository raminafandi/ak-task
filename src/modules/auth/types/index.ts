type AuthState = {
  isLoggedIn: boolean;
  user: {
    uuid: string;
    name: string;
    email: string;
  };
  token: string;
};

type LoginInitialState = {
  email: string;
  password: string;
};

type RegisterInitialState = {
  email: string;
  password: string;
  name: string;
  passwordConfirmation: string;
};

type OTPInitialState = {
  code: string;
};

export { AuthState, LoginInitialState, RegisterInitialState, OTPInitialState };
