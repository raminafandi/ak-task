import AuthNavigator from "@auth/navigation/AuthNavigator";
import { RootNavigator } from "@home/navigation/RootNavigator";
import { NavigationContainer } from "@react-navigation/native";
import React, { FunctionComponent } from "react";

import { useTypedSelector } from "@shared/hooks";

const ContainerNavigator: FunctionComponent = (props) => {
  const { isLoggedIn } = useTypedSelector((state) => state.auth);

  return (
    <NavigationContainer>
      {!isLoggedIn ? <AuthNavigator /> : <RootNavigator />}
    </NavigationContainer>
  );
};

export default ContainerNavigator;
