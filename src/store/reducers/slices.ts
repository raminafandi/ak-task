import { authSlice } from "@auth/store";

export const sliceReducers = {
  auth: authSlice,
};
