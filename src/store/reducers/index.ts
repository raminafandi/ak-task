import { combineReducers } from "@reduxjs/toolkit";

import { sliceReducers } from "./slices";

export const reducers = combineReducers({
  ...sliceReducers,
});
