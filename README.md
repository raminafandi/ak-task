## Getting Started

### Prerequisites

To run this app, you'll need to have the following software installed on your computer:

- Node.js
- Expo CLI

### Installing

1. Clone this repository to your local machine:

```bash
git clone https://gitlab.com/raminafandi/ak-task
```

2. Navigate to the project folder:

```bash
cd ak-task
```

3. Install the project dependencies:

```bash
yarn install
```

### Running

1. Start the Expo development server:

```bash
yarn start
```

2. Open the Expo app on your mobile device.

3. Scan the QR code displayed in the terminal or in the Expo DevTools browser window.

4. Wait for the app to load on your device.

5. Voila! You should now see the app running on your device.

## Built With

- React Native
- Expo
- Redux Toolkit
