module.exports = function (api) {
  api.cache(true);
  return {
    presets: ["babel-preset-expo"],
    plugins: [
      [
        "module-resolver",
        {
          extensions: [".ios.js", ".android.js", ".js", ".ts", ".tsx", ".json"],
          root: ["."],
          alias: {
            "@assets": "./src/assets",
            "@modules": "./src/modules",
            "@shared": "./src/modules/shared",
            "@auth": "./src/modules/auth",
            "@home": "./src/modules/home",
            "@navigation": "./src/navigation",
            "@store": "./src/store",
          },
        },
      ],
      ["react-native-reanimated/plugin"],
    ],
  };
};
